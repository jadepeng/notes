---
title:  K8S 安装与维护
tags:  [k8s]
grammar_cjkRuby: true
date: 2021-07-13 10:35
---


## 初始安装

基于kubeasz安装K8S，安装机器172.31.161.33。

### 下载ezdown：

	export release=3.1.0
	curl -C- -fLO --retry 3 https://github.com/easzlab/kubeasz/releases/download/${release}/ezdown
	chmod +x ./ezdown

### 缓存安装包

默认下载最新推荐k8s/docker等版本（更多关于ezdown的参数，运行./ezdown 查看）

	./ezdown -D

### 启动安装容器

	./ezdown -S

### 进入容器

	docker exec -it kubeasz ezctl start-aio

### 创建集群配置实例

	ezctl new datahub-dev
	
集群配置文件在`/etc/kubeasz/clusters/datahub-dev`
	
先设置参数允许离线安装

	sed -i 's/^INSTALL_SOURCE.*$/INSTALL_SOURCE: "offline"/g'config.yml 

hosts文件，配置集群服务器：

``` javascript
# 'etcd' cluster should have odd member(s) (1,3,5,...)
[etcd]
172.31.160.52 NODE_NAME=master01
172.31.160.52 NODE_NAME=master02
172.31.103.110 NODE_NAME=172
# master node(s)
[kube_master]
172.31.103.110
172.31.160.52
172.31.160.53

# work node(s)
[kube_node]
172.31.103.110
172.31.160.52
172.31.160.53
172.31.160.59
172.31.160.60

# [optional] harbor server, a private docker registry
# 'NEW_INSTALL': 'true' to install a harbor server; 'false' to integrate with existed one
[harbor]
#192.168.1.8 NEW_INSTALL=false

# [optional] loadbalance for accessing k8s from outside
[ex_lb]
#192.168.1.6 LB_ROLE=backup EX_APISERVER_VIP=192.168.1.250 EX_APISERVER_PORT=8443
#192.168.1.7 LB_ROLE=master EX_APISERVER_VIP=192.168.1.250 EX_APISERVER_PORT=8443

# [optional] ntp server for the cluster
[chrony]
#192.168.1.1

[all:vars]
# --------- Main Variables ---------------
# Secure port for apiservers
SECURE_PORT="6443"

# Cluster container-runtime supported: docker, containerd
CONTAINER_RUNTIME="docker"

# Network plugins supported: calico, flannel, kube-router, cilium, kube-ovn
CLUSTER_NETWORK="calico"

# Service proxy mode of kube-proxy: 'iptables' or 'ipvs'
PROXY_MODE="ipvs"

# K8S Service CIDR, not overlap with node(host) networking
SERVICE_CIDR="10.69.0.0/16"

# Cluster CIDR (Pod CIDR), not overlap with node(host) networking
CLUSTER_CIDR="170.22.0.0/16"

# NodePort Range
NODE_PORT_RANGE="20000-40000"

# Cluster DNS Domain
CLUSTER_DNS_DOMAIN="cluster.local"

# -------- Additional Variables (don't change the default value right now) ---
# Binaries Directory
bin_dir="/opt/kube/bin"

# Deploy Directory (kubeasz workspace)
base_dir="/etc/kubeasz"

# Directory for a specific cluster
cluster_dir="{{ base_dir }}/clusters/datahub-dev"

# CA and other components cert/key Directory
ca_dir="/etc/kubernetes/ssl"

```

### 安装集群

在容器里执行：

	ezctl setup datahub-dev all

等待安装完成。

## 增加node

登录到172.31.161.33，首先配置 ssh 免密码登录新增节点

	ssh-copy-id $new-node-ip
	
然后执行：
	
	 ezctl add-node datahub-dev $new-node-ip

## 删除node

	 ezctl del-node datahub-dev $new-node-ip

## 常用工具

### dashboard

安装：

``` shell
# 部署dashboard 主yaml配置文件
$ kubectl apply -f /etc/ansible/manifests/dashboard/kubernetes-dashboard.yaml
# 创建可读可写 admin Service Account
$ kubectl apply -f /etc/ansible/manifests/dashboard/admin-user-sa-rbac.yaml
# 创建只读 read Service Account
$ kubectl apply -f /etc/ansible/manifests/dashboard/read-user-sa-rbac.yaml
```

登录token：

``` shell
# 创建Service Account 和 ClusterRoleBinding
$ kubectl apply -f /etc/ansible/manifests/dashboard/admin-user-sa-rbac.yaml
# 获取 Bearer Token，找到输出中 ‘token:’ 开头那一行
$ kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

新集群访问地址：[https://172.31.103.110:32779/](https://172.31.103.110:32779/)

token:

	eyJhbGciOiJSUzI1NiIsImtpZCI6InJIWnNncDdycWQ5UHRQbGxwQWFwVVQ3d3M0TmxkRjVFMWhKRjRYZkY4SmcifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWJrYmt3Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIzOGExODlmMi1jZDVjLTQ4NjgtODUzOS03YzQzODVlOGE1YTQiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.eN01mQiZMRJuqCf2isuregXQxONq6VuTKQekxRdqtdnMTJBsq0ZoAuxwQ4tLAtvsccZEYiwsjmniugY_12wci2BONEyzpkiHRYW1mH0KFucgk3DqAy6JDo5uH2TipLN7-HLoyHVcoTP442m_C80Pkr4MyR8ZcRoJGeeSGWp_ZkYSMf1dhMDqMJyZkPMJJ70zvnYQ98NHYbt1t2KYNRuuMXVbvICuVZdE6mNizDuopQ7DBkqJKGy5T-JBdyjrHOCEKZPTXUEEFsvh-Q9cv5ohmDxiDnC24CguA6wBolG7UHADR5ZWwVExLN5R2w2EXBCxPUafU3ULmXzezKsKXinepA


## 参考资料

官方文档： https://github.com/easzlab/kubeasz


--------------------------

感谢您的认真阅读。

如果你觉得有帮助，欢迎点赞支持！

不定期分享软件开发经验，欢迎关注作者, 一起交流软件开发：


