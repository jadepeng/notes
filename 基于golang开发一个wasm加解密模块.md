---
title: 基于golang开发一个wasm加解密模块
tags: 
grammar_cjkRuby: true
---


为保障数据安全，网页上显示的图片，音频等敏感信息，需要做加密，在网页端调用wasm解密，然后渲染出来。
Golang原生支持编译为wasm，所以开发会比较便捷，选型时选择golang。
整体思路：
	
		js请求文件buffer -> buffer传给wasm -> wasm调用解密函数解密 -> js渲染解密后数据
		
## golang wasm解密模块

开发wasm，需要先引用 "syscall/js"

函数需要

```golang
func decode(this js.Value, args []js.Value) interface{} {
	// 加密key
	keys := ([]byte)("KEY_PREFIX_" + args[0].String())
	buffer := make([]byte, args[1].Length())

	// 从js读取
	js.CopyBytesToGo(buffer, args[1])

	// ^ 示例解密函数 （ a ^ b ^ b = a)
	for i := range buffer {
		buffer[i] = buffer[i] ^ keys[i%len(keys)]
	}

	// 拷贝到js
	array := js.Global().Get("Uint8Array").New(len(buffer))
	js.CopyBytesToJS(array, buffer)

	return array
}

```

注意点：
- js和golang传递buffer，需要经理CopyBytesToGo和CopyBytesToJS两个过程
- args 是js调用的函数

main里，需要导出decode函数

```golang
func main() {
	done := make(chan int, 0)
	js.Global().Set("decode", js.FuncOf(decode))
	<-done
}
```

## js 调用

先引用`wasm_exec.js` 胶水代码

    <script src="wasm_exec.js"></script>


然后，加载wasm解密：

```
function decodeImage() {
	var oReq = new XMLHttpRequest();
	oReq.open("GET", "demo_encode.png", true);
	oReq.responseType = "arraybuffer";

	oReq.onload = function (oEvent) {
		var arrayBuffer = oReq.response; // 注意:不是oReq.responseText
		if (arrayBuffer) {
			var byteArray = new Uint8Array(arrayBuffer);
			var decodeData = decode('KEY_HERE', byteArray);
			document.getElementById("decodeJavaImage").src = URL.createObjectURL(
				new Blob([decodeData], { type: 'image/png' })
			);
		}
	};
	oReq.send(null);
 }
```



--------------------------

感谢您的认真阅读。

如果你觉得有帮助，欢迎点赞支持！

不定期分享软件开发经验，欢迎关注作者, 一起交流软件开发：
