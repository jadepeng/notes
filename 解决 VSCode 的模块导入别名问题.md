---
title: 解决 VSCode 的模块导入别名问题
tags: 
grammar_cjkRuby: true
---

添加`jsconfig.json`

```json
{
  "compilerOptions": {
    "target": "es2015",
    "baseUrl": "./src",
    "paths": {
      "@api/*": ["api/*"],
      "@common/*": ["common/*"],
      "@components/*": ["components/*"],
      "@doc/*": ["../doc/*"],
      "@domain/*": ["domain/*"],
      "@router/*": ["router/*"],
      "@model/*": ["domain/model/*"],
      "@service/*": ["domain/service/*"],
      "@store/*": ["store/*"],
      "@utils/*": ["utils/*"],
      "@views/*": ["views/*"],
    },
    "allowSyntheticDefaultImports": true
  },
  "exclude": ["node_modules", "dist"]
}

```

- 注意下\*，之前没加没有生效
- exclude 可以加速

`paths`配置alias，这样就可以在代码里使用alias

```javascript
import createRouter from '@/router';
import store from '@/store';
```




--------------------------

感谢您的认真阅读。

如果你觉得有帮助，欢迎点赞支持！

不定期分享软件开发经验，欢迎关注作者, 一起交流软件开发：
