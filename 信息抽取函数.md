---
title: 信息抽取函数
tags: 
grammar_cjkRuby: true
---

## 功能介绍

信息抽取函数是工作流中，用于对结构化数据进行处理的工具。

在工作流的结构化数据处理工具中。

![处理工具](./images/1656396962145.png)


## 使用说明

结构化抽取适用于json格式的基础数据，选择结构化抽取方式信息抽取时，可以通过信息抽取函数对数据进行转换。

在通过结构化抽取方式进行信息抽取时，即用交互界面配置或用代码编辑进行信息抽取时，需要配置引用字段的抽取函数。

KG服务支持的信息抽取函数如下表所示 所示。
| 抽取函数 | 函数说明 | 抽取函数示例 | 抽取前数据示例 | 抽取后数据示例 |
| --- | --- | --- | --- | --- |
| trim（字段） | 裁剪字段前后的空白字符 | trim(${name}) | "name":" mike " | "mike" |
| substring (字段, int pos, int len） | 获取从该字段值第pos个字符（从0开始）开始的长度为len的子串 | substring (${id}, 1, 6) | "id":"a000111" | "000111" |
| split(字段,string pattern ) | 将字段值以pattern为分隔符分割，得到一个列表。pattern可以是一个正则表达式，所有与pattern匹配的子串都作为分隔符。 | split(${roles}, ', ') | "roles":"role1,role2" | ["role1", "role2"] |
| concat（字段1,字段2, ...） | 将字段1、字段2...依次拼接成一个长字符串。此处字段值也可以是一个字符串常量。 | concat(${name}, '--', ${id}) | "name": "mike","id": "a000111" | "mike--a000111" |
| regexp_replace（字段, string pattern, string replacement） | 将字段值中与正则表达式pattern匹配的部分替换为replacement | regexp_replace(${info}, 'hello', 'world') | "info":"helloworld" | "worldworld" |
| upper（字段） | 将字段中英文字母全部转为大写 | upper(${name}) | "name":"mike" | "MIKE" |
| lower（字段） | 将字段中英文字母全部转为小写 | lower(${name}) | "name":"JOHN" | "john" |
| initcap（字段） | 将字段中英文首字母转为大写 | initcap(${city}) | "city":"hangzhou" | "Hangzhou" |
| coalesce（字段1,字段2, ...） | 返回字段1,字段2, ...中第一个不为空的字段 | coalesce(${name},${名字}, ${中文名} ...) | "name":" ","名字":" ","中文名”:"小明” | “小明” |
| parse_array（字段） | 尝试将字段值解析为一个列表 | parse_array(${list}) | "list":["item1","item2"] | ['item1','item2'] |
| array_get（字段, int index） | 获取列表类型的字段中的第index个值（index从0开始） | array_get(parse_array(${list}), 0) | "list":["item1","item2"] | "item1" |
| regexp_extract_all（字段, string pattern, int index） | 提取出字段值中与正则表达式pattern匹配的子串。第三个参数index为可选参数，表示获取正则表达式匹配到的第index个捕获组。注意，index=0表示返回整个匹配的子串，index=1表示返回第一个捕获组。 | regexp_extract_all（${info}, '(he)ll(o)', 1） | "info":"helloworld" | "he" |
其中，“字段”表示基础数据中的字段名，在抽取函数中引用字段时，使用格式为“${字段}”，例如抽取基础数据中属性“name”的字段，在抽取函数中引用字段的格式为“trim(${name})”。


举例：

json文件:

```
{"名称":"痛风","治疗方法":"药物治疗","别名":"关节石病,尿酸盐贮积病,足痛风,历节风}

```
比如上面的`别名`需要转换为数组，可以配置


	别名：split(${别名},",")
	
	
处理后：

```
{"别名":["关节石病","尿酸盐贮积病","足痛风","历节风"],"名称":"痛风"}

```

--------------------------

感谢您的认真阅读。

如果你觉得有帮助，欢迎点赞支持！

不定期分享软件开发经验，欢迎关注作者, 一起交流软件开发：
