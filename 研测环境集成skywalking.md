---
title: 研测环境集成skywalking
grammar_cjkRuby: true
---

## 集成方案

 ### Dockerfile修改

底包使用：

```
 FROM artifacts.iflytek.com/docker-private/datahub/dragonwell8:8.11.12
```


### entrypoint.sh


增加脚本：

```

# env
if [ ! "$ENABLE_APM" ]; then
  echo "NOT ENABLE APM"
else
  export JAVA_OPTS="$JAVA_OPTS  -javaagent:/skywalking/agent/skywalking-agent.jar"
fi

```

当传入ENABLE_APM时，会启用agent

### deployment.yaml 配置

部署文件里环境变量增加skywalking相关配置, 注意修改`SW_AGENT_NAME`为服务名称，其他不用修改

```yaml
          env:
            - name: SW_AGENT_NAME
              value: aimindgraph-rest
            - name: SW_AGENT_COLLECTOR_BACKEND_SERVICES
              value: skywalking-oap.incubation:11800
            - name: SW_AGENT_NAMESPACE
              value: incubation
            - name: ENABLE_APM
              value: 'true'
```


## SK 地址

访问地址： https://apmtest.iflyresearch.com/general

## 高级应用

### 日志与trace关联

首先，根据采用的日志系统引入maven依赖，比如log4j2：

```
   <dependency>
      <groupId>org.apache.skywalking</groupId>
      <artifactId>apm-toolkit-log4j-2.x</artifactId>
      <version>{project.release.version}</version>
   </dependency>
```

当期最新的是`8.11.0`

修改`log4j2.xml` 配置文件，增加`traceid`

```

        <Console name="console" target="SYSTEM_OUT">
            <ThresholdFilter level="DEBUG" onMatch="ACCEPT" onMismatch="DENY"/>
            <PatternLayout charset="${log-charset}"
                           pattern="%-d{yyyy-MM-dd HH:mm:ss} [%traceId] [%t] [%-5p] %c{1.} %x - %m%n"/>
        </Console>
```

使用logback的见： https://skywalking.apache.org/docs/skywalking-java/latest/en/setup/service-agent/java-agent/application-toolkit-logback-1.x/

